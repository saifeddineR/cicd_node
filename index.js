const express = require("express");
const app = express();

app.get("/", (req, res) =>
  res.json({ msg: "welcome to my CICD project using gitlab ci" })
);

app.listen(5000, () => console.log("server is running on port 5000"));
